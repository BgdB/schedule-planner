<?php
namespace ScheduleApp\FOSUserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ScheduleApp\MainBundle\Entity\User;

class RegistrationController extends BaseController
{
    /**
     * @param Request $request
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        if ($this->container->get('security.context')->getToken()->getUser() instanceof User) {
            return new RedirectResponse($url = $this->container->get('router')->generate('main_homepage'));
        }

        return parent::registerAction($request);
    }
}