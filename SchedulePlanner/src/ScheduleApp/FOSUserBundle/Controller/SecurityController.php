<?php


namespace ScheduleApp\FOSUserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ScheduleApp\MainBundle\Entity\User;

class SecurityController extends BaseController
{
    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        if ($this->container->get('security.context')->getToken()->getUser() instanceof User) {
            return new RedirectResponse($url = $this->container->get('router')->generate('main_homepage'));
        }

        return parent::loginAction($request);
    }
}