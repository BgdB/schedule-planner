<?php

namespace ScheduleApp\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserController extends FOSRestController
{
    /**
     * This method, returns all users if $id = null. If $id != null, returns the user with user id = $id.
     * If there is no user with that id, it returns null.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Returns all users if $id is null, else returns the user with user.id = id (if exists)",
     * )
     */
    public function getUserAction($id = null)
    {
        $repository = $this->getDoctrine()->getRepository("MainBundle:User");

        $data = ($id == null) ?
            $repository->findAll() :
            $repository->findOneById($id);

        $serializer = SerializerBuilder::create()->build();

        return new JsonResponse($serializer->serialize($data, 'json'));
    }
}
